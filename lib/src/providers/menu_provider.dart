import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';




class _MenuProvider{
  List<dynamic> opciones = [];
  _MenuProvider(){
    cargaData();
  }

  Future<List<dynamic>> cargaData() async {
    final resp = await rootBundle.loadString('data/menu_opts.json');
    Map dataMAp = json.decode(resp);
    opciones = dataMAp['rutas'];
    return opciones;
  }
}

final menuProvider = new _MenuProvider(); 