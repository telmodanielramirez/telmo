import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget{

  final opciones = ['uno','dos','tres','cuatro','cinco'];

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(''),
      ),
      body: ListView(
        children: _crearItemsCortos()
        
      ),
    );
  }

  List<Widget> _crearItems(){
    List<Widget> lista = new List<Widget>();
    for (String opt in opciones){
      final tempWidget = ListTile(
        title: Text(opt),
      );
      lista..add(tempWidget)
           ..add(Divider());
      }
    return lista;
  }


  List<Widget> _crearItemsCortos(){
    return opciones.map((e) => Column(
      children: <Widget>[
        ListTile(
          title: Text(e + '7'),
          subtitle: Text('cualquier cosa'),
          leading: Icon(Icons.account_balance_wallet),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: (){},
        ),
        Divider()
      ],
    )
    ).toList();
  }

}