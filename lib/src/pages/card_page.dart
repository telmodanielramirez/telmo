import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text('Card'),
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
          _cardTipo2()
        ],
      ),
    );
  }

  Widget _cardTipo1() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.alarm),
            title: Text('telmo'),
            subtitle: Text('Esto ya tiene que ser un texto muy largo para que se vcea la diferencia de widget'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                onPressed: (){},
                child: Text('Cancelar'),
              ),
              FlatButton(
                onPressed: (){}, 
                child: Text('Ok'),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo2(){
    return Card(
      child: Column(
        children: <Widget>[
          FadeInImage(
            placeholder: AssetImage('assets/jar-loading.gif'), 
            image: NetworkImage('https://media.istockphoto.com/photos/summer-hilly-landscape-with-dark-heavy-clouds-flowing-in-the-sky-picture-id1153941026')
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('No tengo idea  que poner'),
          )
        ],
      ),
    );
  }


}